# Fullstack developer (MEAN)

## Explicación

El test consiste en crear un formulario de registro (Signup) de usuarios. Como información personal se debe contemplar:

- Nombre
- Apellidos
- RUT (http://cauditor2.blogspot.com/2009/09/modulo-11-o-validador-de-rut.html)
- Email
- Región (https://www.bcn.cl/siit/nuestropais/regiones)
- Comuna (https://www.bcn.cl/siit/nuestropais/regiones)
- Dirección
- Fecha de nacimiento
- Foto de perfil

## Requerimientos

1. Se deben validar los campos según correspondan. **Todos los campos son requeridos, salvo la foto de perfil del usuario, por defecto proporcionar una**. En caso de error indicar el error y marcar el campo como inválido. No debe permitir realizar submit si existen errores.
2. Si el usuario es menor a 18 años, debe mostrar una alerta y no permitir el registro.
3. Consumir servicio **GET /regiones-y-comunas** para obtener las regiones y sus comunes. Considerar estrategia para hacer un cache de la información que se obtiene del servicio y que tenga expiración.
4. Filtrar regiones y comunas a medida que se tipea el nombre.
5. Si el usuario que se registra no es de la Región Metropolitana, mostrar alerta con mensaje y permitir registro.
6. Si el usuario que se registra pertenece a la comuna de "Las condes" o "Santiago", mostrar alerta indicando que se alcanzó el límite de inscripción para esas comunas y no permitir registro.
7. Limitar el tamaño máximo de la foto de perfil que sube el usuario a 2MB.
8. El layout debe adaptarse al tamaño del dispositivo que despliega la app.
9. Tomar los datos recolectados del formulario y enviarlos al servicio **POST /users**.

### Datos servicio:

- **url:** https://e0a248af-6cfd-44d6-bec2-6c97ee008709.mock.pstmn.io
- **apiKey:** PMAK-5f0cbd4131291e00427974ae-e4857304d35dea5d9f1875e38965c758d8
- **nombre header apiKey:** x-api-key

## Requisitos técnicos de la solución

1. Libertad para escoger cualquier framework frontend, pero ganas puntos si utilizas Angular.
2. Utilizar alguna librería de front, puntos a tu favor si utilizas materialize o bootstrap.
3. Usar docker para levantar la app.
4. Si le agregas testing ganas puntos.
5. Breve README.md con explicación de la solución y herramientas utilizadas.

## Formato de entrega

Se debe enviar un pull request (PR) a este repo.

El PR debe incluir instrucciones de cómo levantar los contenedores para visualizar el proyecto correctamente. Indicar en quÃ© puerto corre la app.